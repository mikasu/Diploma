import java.util.ArrayList;

public class Main {


    private static final String RACES_FILE_NAME = "races.log";
    private static ArrayList<Race> races;

    public static void main(String[] args) {
        System.out.println("Запуск...");

        System.out.println("Импорт заездов");
        RacesImport ri = new RacesImport();
        races = ri.loadFromFile();
        System.out.println("Заезды успешно импортированы\n" +
                "Найдено " + races.size() + " зездов");

        System.out.println("Запуск наблюдения за процессом");
        Thread processControl = new Thread(new RacesWatcher());
        processControl.start();

        //Создание меню в трее
        //TODO Создать меню в системном трее []
    }

    public static void addRace(Race race){
        races.add(race);
    }

    public static String getRacesFileName() {
        return RACES_FILE_NAME;
    }
}
