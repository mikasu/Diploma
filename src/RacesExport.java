import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

class RacesExport {

    private final String DEFAULT_FILE_NAME;
    private String fileName;

    RacesExport(String fileName){

        DEFAULT_FILE_NAME = "races.log";
        if (!fileName.isEmpty()){
            this.fileName = fileName;
        }
        else
        {
            this.fileName = DEFAULT_FILE_NAME;
        }

    }

    void addRace(Race race) {

        Enigma anEnigma = new Enigma(null);
        String enCryptedRace = anEnigma.encrypt(race.toString()) + "\n";
        try {
            List<String> lines = Collections.singletonList(enCryptedRace);
            Path file = Paths.get(fileName);
            Files.write(file, lines, Charset.forName("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
