public interface Status {
    static final String CANCELED = "Canceled";
    static final String SUCCESS = "Success";
}
