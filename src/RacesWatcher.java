import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Date;

public class RacesWatcher implements Runnable{
    private final String processName = ""; //TODO Добавить имена процессов
    private boolean processActive = false;
    Race race;

    @Override
    public void run() {
        System.out.println("Наблюдение за процессом запущено");
        //TODO Реализовать слежение за списком процессов
    }

    private ArrayList<String> getProcessList(){
        ArrayList<String> processes= new ArrayList<String>();
        //TODO Реализовать получение списка запущенных процессов
        return processes;
    }

    private void onProccessFound(){
        if (!processActive){
            race.setStartedAt(String.valueOf(System.currentTimeMillis())); //Устанавлиает время старта
            processActive = true;
        }
    }

    private void onProccessDontFound(){
        if (processActive){
            race.setFinishedAt(String.valueOf(System.currentTimeMillis())); //Устанавливает время финиша
            race.setLength(String.valueOf(
                    Long.parseLong(race.getFinishedAt()) - Long.parseLong(race.getStartedAt()) //Устанавливает длительность заезда
            ));

            //TODO Добавить остальные данные для Race здесь <----

            processActive = false;

            Main.addRace(race);
            RacesExport re = new RacesExport(Main.getRacesFileName());
            re.addRace(race);
        }
    }
}
