import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class RacesImport {

    private String fileName = Main.getRacesFileName();

    public ArrayList<Race> loadFromFile(){
        ArrayList<Race> races = new ArrayList<Race>();
        //Чтение из фала всех строк (строки зашифрованы)
        List<String> fileLines = new ArrayList<String>();

        Path path = Paths.get(fileName);
        try{
            fileLines = Files.readAllLines(path);

        }catch(IOException ex){
            ex.printStackTrace(); //handle exception here
        }

        //Расшифровка строк (Расшифровал - положил в коллекцию
        Enigma anEnigma = new Enigma(null);

        fileLines.forEach(line -> {
            Race race = new Race();

            line = line.replaceAll("\n", "");
            line = anEnigma.decrypt(line);

            String[] raceArray = line.split(",");
            race.setDate(raceArray[0]);
            race.setStartedAt(raceArray[1]);
            race.setFinishedAt(raceArray[2]);
            race.setLength(raceArray[3]);
            race.setOperatorId(raceArray[4]);
            race.setStatus(raceArray[5]);

            races.add(race);
        });

        //Возвращает заезды в виде коллекции Race
        return races;
    }
}
