/**
 * Name: Модуль криптографии Энигма
 * License: MIT
 * Author: Dmitry Korolev
 * Date: 12.19.2018 23:02
 */

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class Enigma {

    private String key; // 128 bit key
    private String initVector = "RandomInitVector"; // 16 bytes IV

    public Enigma (String key){

        if (!key.isEmpty() && key.length() < 16) {
            this.key = "Nar13145Har41345";
        }
        else
        {
            this.key = key;
        }
    }

    public String decrypt(String encrypted) {
        try {
            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

            byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted));

            return new String(original);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public String encrypt(String value) {
        try {
            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

            byte[] encrypted = cipher.doFinal(value.getBytes());
            System.out.println("encrypted string: "
                    + Base64.encodeBase64String(encrypted));

            return Base64.encodeBase64String(encrypted);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    /**
     * Метод проверки работоспособности модуля криптографии
     * @return
     */
    public boolean test(){

        String message = "Test message, encrypted with KEY = " + key;
        String encryptedString = encrypt(message);
        String decryptedString = decrypt(encryptedString);

        if (encryptedString != null && decryptedString != null) {
            return message.equals(decryptedString) && !encryptedString.equals(message);
        }
        return false;
    }

}
